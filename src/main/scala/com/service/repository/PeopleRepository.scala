package com.service.repository

import com.service.dto.Person
import com.service.schema.PeopleTable
import slick.dbio.DBIOAction
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Future

class PeopleRepository(implicit db: Database) extends DatabaseRepository[Person] {
  val peopleQuery = TableQuery[PeopleTable]

  override def initRepo(): Future[Unit] = {
    db.run(DBIOAction.seq(
      peopleQuery.schema.createIfNotExists
    ))
  }

  override def save(data: Person): Future[Int] = {
    db.run(
      peopleQuery += data
    )
  }

  override def update(data: Person): Future[Int] = {
    db.run(
      peopleQuery.filter(_.id === data.id)
        .map(b => (b.firstName, b.secondName, b.birthday, b.address))
        .update((data.firstName, data.secondName, data.birthday, data.address))
    )
  }

  override def delete(id: Int): Future[Int] = {
    db.run(
      peopleQuery.filter(_.id === id).delete
    )
  }

  override def get(id: Int): Future[Seq[Person]] = {
    db.run(
      peopleQuery.filter(_.id === id).result
    )
  }

  def getAll: Future[Seq[Person]] = {
    db.run(
      peopleQuery.result
    )
  }
}
