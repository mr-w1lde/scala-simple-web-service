package com.service.repository

import scala.concurrent.Future

trait DatabaseRepository[T] {

  def initRepo(): Future[Unit]

  def save(data: T): Future[Int]
  def update(data: T): Future[Int]
  def delete(id: Int): Future[Int]
  def get(id: Int): Future[Seq[T]]

}
