package com.service.schema

import java.sql.Date

import com.service.dto.Person
import slick.jdbc.PostgresProfile.api._
import slick.sql.SqlProfile.ColumnOption.SqlType


// Definition of the People table
class PeopleTable(tag: Tag)
  extends Table[Person](tag,"People") {

  // This is the primary key column
  // Id has to be as SERIAL type for AutoInc (PostgresSql)
  def id = column[Int]("id", SqlType("SERIAL"), O.PrimaryKey, O.AutoInc)
  def firstName = column[String]("first_name")
  def secondName = column[String]("second_name")
  def birthday = column[Date]("birthday")
  def address = column[String]("address")

  override def * = (id.?, firstName, secondName, birthday, address) <> (Person.tupled, Person.unapply)

}
