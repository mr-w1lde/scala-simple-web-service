package com.service.actor.request

import akka.http.scaladsl.model.StatusCodes
import com.service.actor.PerRequestActor
import com.service.actor.PersonManager.UpdatePerson
import com.service.repository.PeopleRepository

import scala.util.{Failure, Success}

trait PersonUpdater {
  this: PerRequestActor =>

  def peopleRepository: PeopleRepository

  def updatePerson(request: UpdatePerson): Unit = {
    peopleRepository.update(request.person).onComplete {
      case Success(value) => resultByValue(value, request)
      case Failure(exception) =>
        log.error("Couldn't update person = {}", exception)
        complete("Internal Error", StatusCodes.InternalServerError)
    }
  }

  def resultByValue(value: Int, request: UpdatePerson): Unit = value match {
    case 0 => complete("Person not found", StatusCodes.NotFound)
    case 1 => complete(s"Person with id<${request.person.id}> has been updated", StatusCodes.OK)
    case _ => complete("Unknown error", StatusCodes.InternalServerError)
  }
}
