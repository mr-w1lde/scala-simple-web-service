package com.service.actor.request

import akka.http.scaladsl.model.StatusCodes
import com.service.actor.PerRequestActor
import com.service.actor.PersonManager.SavePerson
import com.service.repository.PeopleRepository

import scala.util.{Failure, Success}

trait PersonSaver {
  this: PerRequestActor =>

  def peopleRepository: PeopleRepository

  def savePerson(request: SavePerson): Unit = {
    peopleRepository.save(request.person).onComplete {
      case Success(value) =>
        complete("Person saved", StatusCodes.Created)
      case Failure(exception) =>
        log.error("Couldn't save person = {}", exception)
        complete("Internal Error", StatusCodes.InternalServerError)
    }
  }
}
