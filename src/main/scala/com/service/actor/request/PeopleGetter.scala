package com.service.actor.request

import akka.http.scaladsl.model.StatusCodes
import com.service.actor.PerRequestActor
import com.service.repository.PeopleRepository

import scala.util.{Failure, Success}

trait PeopleGetter {
  this: PerRequestActor =>

  def peopleRepository: PeopleRepository

  def getPeople(): Unit = {
    peopleRepository.getAll.onComplete {
      case Success(value) => complete(value, StatusCodes.OK)
      case Failure(exception) =>
        log.error("Couldn't get people = {}", exception)
        complete("Internal Error", StatusCodes.InternalServerError)
    }
  }
}
