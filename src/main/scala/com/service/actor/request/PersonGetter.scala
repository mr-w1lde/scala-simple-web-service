package com.service.actor.request

import akka.http.scaladsl.model.StatusCodes
import com.service.actor.PerRequestActor
import com.service.actor.PersonManager.GetPerson
import com.service.dto.Person
import com.service.repository.PeopleRepository

import scala.util.{Failure, Success}

trait PersonGetter {
  this: PerRequestActor =>

  def peopleRepository: PeopleRepository

  def getPerson(request: GetPerson): Unit = {
    peopleRepository.get(request.id).onComplete {
      case Success(value) => resultByValue(value)
      case Failure(exception) =>
        log.error("Couldn't get person = {}", exception)
        complete("Internal Error", StatusCodes.InternalServerError)
    }
  }

  def resultByValue(value: Seq[Person]): Unit = {
    if(value.isEmpty) {
      complete("Person not found", StatusCodes.NotFound)
    } else {
      complete(value.head, StatusCodes.OK)
    }
  }
}
