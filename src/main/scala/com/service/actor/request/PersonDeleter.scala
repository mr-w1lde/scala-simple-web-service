package com.service.actor.request

import akka.http.scaladsl.model.StatusCodes
import com.service.actor.PerRequestActor
import com.service.actor.PersonManager.DeletePerson
import com.service.repository.PeopleRepository

import scala.util.{Failure, Success}

trait PersonDeleter {
  this: PerRequestActor =>

  def peopleRepository: PeopleRepository

  def deletePerson(request: DeletePerson): Unit = {
    peopleRepository.delete(request.id).onComplete {
      case Success(value) => resultByValue(value)
      case Failure(exception) =>
        log.error("Couldn't delete person = {}", exception)
        complete("Internal Error", StatusCodes.InternalServerError)
    }
  }

  def resultByValue(value: Int): Unit = value match {
    case 0 => complete("Person not found", StatusCodes.NotFound)
    case 1 => complete("Person deleted", StatusCodes.OK)
    case _ => complete("Unknown error", StatusCodes.InternalServerError)
  }
}
