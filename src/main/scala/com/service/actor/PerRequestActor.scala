package com.service.actor

import akka.actor.{Actor, ActorLogging}
import com.service.routing.PerRequest
import com.service.serialization.JsonSupport

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

trait PerRequestActor
  extends Actor
  with ActorLogging
  with PerRequest
  with JsonSupport {

  context.setReceiveTimeout(60 seconds)

  implicit val ec: ExecutionContextExecutor = context.dispatcher

}
