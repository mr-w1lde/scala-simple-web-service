package com.service.actor

import akka.actor.Props
import com.service.actor.request.{PeopleGetter, PersonDeleter, PersonGetter, PersonSaver, PersonUpdater}
import com.service.dto.Person
import com.service.repository.PeopleRepository
import akka.http.scaladsl.server.{RequestContext, RouteResult}

import scala.concurrent.Promise

object PersonManager {

  //Initializer for Akka Actor
  def props(peopleRepository: PeopleRepository,
            requestContext: RequestContext,
            routeResult: Promise[RouteResult]): Props =
    Props(new PersonManager(peopleRepository, requestContext, routeResult))
  
  sealed trait PersonRequest

  case class SavePerson(person: Person) extends PersonRequest
  case class UpdatePerson(person: Person) extends PersonRequest
  case class DeletePerson(id: Int) extends PersonRequest
  case class GetPerson(id: Int) extends PersonRequest
  case class GetPeople() extends PersonRequest

}

class PersonManager(val peopleRepository: PeopleRepository,
                    val requestContext: RequestContext,
                    val routeResult: Promise[RouteResult])
  extends PerRequestActor
    with PersonSaver
    with PersonUpdater
    with PersonDeleter
    with PersonGetter
    with PeopleGetter {
  import PersonManager._

  //Receiving our PersonRequest messages
  override def receive: Receive = {
    case request: PersonRequest => handle(request)
  }

  def handle(request: PersonRequest): Unit = request match {
    case r: SavePerson => savePerson(r)
    case r: UpdatePerson => updatePerson(r)
    case r: DeletePerson => deletePerson(r)
    case r: GetPerson => getPerson(r)
    case _: GetPeople => getPeople()
  }

}
