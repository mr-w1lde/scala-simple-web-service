package com.service.serialization

import java.sql.Date
import java.text.SimpleDateFormat

import spray.json.{JsString, JsValue, JsonFormat, deserializationError}

import scala.util.Try

// Pray Json doesn't know how to convert Date(java.sql.Date) type
// Converting Date to JsString if we want to serialize it
// Converting JsString to Date if we wont to deserialize it
object DateToJson {
  implicit object DateFormat extends JsonFormat[Date] {
    override def write(obj: Date): JsValue = JsString(dateToString(obj))

    override def read(json: JsValue): Date = json match {
      case JsString(value) =>
        parseDateString(value).fold(deserializationError(s"Expected Date format, got $value"))(identity)
      case error => deserializationError(s"Expected JsString, got $error")
    }
  }

  private val localDateFormatter = new ThreadLocal[SimpleDateFormat] {
    override def initialValue() = new SimpleDateFormat("yyyy-MM-dd")
  }

  private def dateToString(date: Date) =
    localDateFormatter.get().format(date)

  private def parseDateString(date: String): Option[Date] =
    Try{
      val temp = localDateFormatter.get().parse(date)
      new Date(temp.getTime)
    }.toOption
}
