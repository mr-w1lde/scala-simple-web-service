package com.service.serialization

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.service.dto.Person
import com.service.serialization.DateToJson.DateFormat
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

// Collect your json format instances into a support trait:
// Use it wherever json (un)marshalling is needed
trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val personFormat: RootJsonFormat[Person] = jsonFormat5(Person)
}
