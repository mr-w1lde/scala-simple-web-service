package com.service.routing

import akka.actor.Actor
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model.StatusCode
import akka.http.scaladsl.server.RouteResult.Complete
import akka.http.scaladsl.server.{RequestContext, RouteResult}

import scala.concurrent.{ExecutionContext, Promise}
import scala.util.{Success, Try}

trait PerRequest {
  this: Actor =>

  implicit def ec: ExecutionContext

  def requestContext: RequestContext
  def routeResult: Promise[RouteResult]

  def complete(response: => ToResponseMarshallable, statusCode: StatusCode):Unit = {
    //Getting ToResponseMarshallable
    val temp = requestContext.complete(response)

    temp.onComplete { res =>
        val result: Try[RouteResult] = res match {
          case Success(value: Complete) => Success {
            value.copy(
              //Copy of our response with StatusCode
              response = value.response.withStatus(statusCode)
            )
          }
          case _ => res
        }

      //Returning RouteResult
      routeResult.complete(result)
    }

    context.stop(self)
  }

}
