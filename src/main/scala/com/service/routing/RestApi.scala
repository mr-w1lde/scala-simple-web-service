package com.service.routing

import akka.actor.ActorSystem
import akka.http.scaladsl.server.{Route, RouteResult}
import akka.http.scaladsl.server.Directives._
import com.service.actor.PersonManager
import com.service.actor.PersonManager.PersonRequest
import com.service.dto.Person
import com.service.repository.PeopleRepository
import com.service.serialization.JsonSupport

import scala.concurrent.Promise

class RestApi(implicit val actorSystem: ActorSystem, implicit val peopleRepository: PeopleRepository)
  extends JsonSupport {
  /**
   * RestApi Routes for the GET/POST/Other REST endpoints for the People/Person endpoints.
   */
  val route: Route =
    concat(
      path("api" / "v1" / "person") {
        parameters("id".as[Int]) { id =>
          get {
            handle(PersonManager.GetPerson(id))
          }~
            delete {
              handle(PersonManager.DeletePerson(id))
            }
        }~
        post {
          entity(as[Person]) { person =>
            handle(PersonManager.SavePerson(person))
          }
        }~
          put {
            entity(as[Person]) { person =>
              handle(PersonManager.UpdatePerson(person))
            }
          }

      },
      path("api" / "v1" / "people") {
        get {
          handle(PersonManager.GetPeople())
        }
      }
    )

  def handle(request: PersonRequest): Route = ctx => {
    val result = Promise[RouteResult]
    val actor = actorSystem.actorOf(PersonManager.props(peopleRepository, ctx, result))
    actor ! request
    result.future
  }

}
