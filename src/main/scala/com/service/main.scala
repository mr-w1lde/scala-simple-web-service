package com.service

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import com.service.repository.PeopleRepository
import com.service.routing.RestApi
import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory
import slick.jdbc.PostgresProfile
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

object main extends App {
  private val logger = LoggerFactory.getLogger("Main")

  //Initializing Akka Actor
  implicit val system: ActorSystem = ActorSystem()
  //Initializing Database form application.conf file
  implicit val db: PostgresProfile.backend.Database = Database.forConfig("database.postgres")
  //Initializing People Repository for Postgres
  implicit val peopleRepository: PeopleRepository = new PeopleRepository()
  //Loading config file (application.conf)
  val config = ConfigFactory.load()

  system.registerOnTermination(() => db.close())

  val host = config.getString("application.host")
  val port = config.getInt("application.port")

  //Checking if Database has 'PeopleTable':
  //If exist when skipping
  //If not exist when creating
  peopleRepository.initRepo().onComplete {
    case Success(_) => logger.info("People repository was successfully initialized")
    case Failure(exception) =>
      logger.error("Init people repository with exception = {}", exception.toString)
      throw exception
  }

  val restApi = new RestApi()

  //Starting web service with our binding
  Http().newServerAt(host, port).bind(restApi.route)

  logger.info(s"Server started at address $host:$port")

}

