package com.service.dto

import java.sql.Date

case class Person(
                   id: Option[Int],
                   firstName: String,
                   secondName: String,
                   birthday: Date,
                   address: String
                 )

