# Scala Simple Web Service

Для сборки проекта и Docker Image использовать sbt

Запуск через Docker:
    - Делаем сборку проекта через sbt: sbt compile
    - После собираем Docker Image через sbt: sbt docker:publishLocal
    - Запускаем Docker-Compose: docker-compose up -d

Сервис запускается на порту 9090
Для просмотра Swagger документации поднимается отдельный контейнер, который доступен по порту 8080
Для тестирования сервиса я использовал Postman

# Stack
Scala + sbt 
Akka & Slick
PostgreSQL
Docker 
Swagger
