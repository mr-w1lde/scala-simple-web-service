name := "ScalaWebService"

version := "0.1"

scalaVersion := "2.13.3"

libraryDependencies ++= {
  val AkkaVersion = "2.6.8"
  val AkkaHttpVersion = "10.2.0"
  val SlickVersion = "3.3.2"
  Seq(
    "com.typesafe.slick" %% "slick" % SlickVersion,
    "com.typesafe.slick" %% "slick-hikaricp" % SlickVersion,
    "com.lightbend.akka" %% "akka-stream-alpakka-slick" % "2.0.1",
    "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
    "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
    "com.typesafe.akka" %% "akka-slf4j"      % AkkaVersion,
    "com.typesafe.akka" %% "akka-actor"      % AkkaVersion,
    "com.typesafe.akka" %% "akka-actor-typed" % AkkaVersion,
    "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion,
    "org.postgresql" % "postgresql" % "42.2.13",
    "ch.qos.logback" % "logback-core" % "1.3.0-alpha5",
    "ch.qos.logback" % "logback-classic" % "1.3.0-alpha5",
    "org.slf4j" % "slf4j-nop" % "1.6.4",
    "org.slf4j" % "slf4j-api" % "2.0.0-alpha1",
    //"se.marcuslonnberg" % "sbt-docker" % "1.8.0"
  )
}

// Need to use full name to DockerPlugin, since sbt-native-packager uses the same name for its Docker plugin
enablePlugins(sbtdocker.DockerPlugin, JavaAppPackaging)

dockerfile in docker := {
  val appDir = stage.value
  val targetDir = "/app"

  new Dockerfile {
    from("openjdk:8-jre")
    entryPoint(s"$targetDir/bin/${executableScriptName.value}")
    copy(appDir, targetDir)
  }
}

buildOptions in docker := BuildOptions(cache = false)